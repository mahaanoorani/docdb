# MyMedtro

## Members

|  Name | UTEID  | GitLab ID  |
|---|---|---|
|Divya Manohar|dm46274|divsman|
|Duncan Huntsinger|dbh875|groverhood|
|Kyle Wang|klw3649|kyle_wang|
|Mahaa Noorani|msn658|mahaanoorani|
|Saad Ahmad|sa46827|saad_ahmad1|

## Git SHA
Phase 1: cb3a20c5a4336b2e61b5e29749e4faff604387dc

Phase 2: e4638709c02a28acce5a9de6e8eea28090b83bef

Phase 3: 7d6aa7ead7c4a8ccad989c97b0411ee835ee87d8

Phase 4: 58b9cab157a8e80d2b341a238ec9db226a314e99

## Project Leader
Phase 1: Duncan Huntsinger

Phase 2: Saad Ahmad

Phase 3: Mahaa Noorani

Phase 4: Divya Manohar

## Gitlab Pipelines
https://gitlab.com/groverhood/docdb/-/pipelines

## Website Link
https://www.mymedtro.com/

## Completion Times

### Phase 1
| Name | Estimated | Actual |
|------|-----------|--------|
|Divya Manohar|5|9|
|Duncan Huntsinger|6|11|
|Kyle Wang|5|6|
|Mahaa Noorani|4|7|
|Saad Ahmad|5|10|

### Phase 2
| Name | Estimated | Actual |
|------|-----------|--------|
|Divya Manohar|15|25|
|Duncan Huntsinger|20|25|
|Kyle Wang|10|10|
|Mahaa Noorani|20|30|
|Saad Ahmad|25|30|

### Phase 3
| Name | Estimated | Actual |
|------|-----------|--------|
|Divya Manohar|15|22|
|Duncan Huntsinger|14|20|
|Kyle Wang|10|10|
|Mahaa Noorani|20|35|
|Saad Ahmad|25|30|

### Phase 4
| Name | Estimated | Actual |
|------|-----------|--------|
|Divya Manohar|5|5|
|Duncan Huntsinger|5|5|
|Kyle Wang|2|3|
|Mahaa Noorani|5|10|
|Saad Ahmad|5|7|
