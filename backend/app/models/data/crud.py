from sqlalchemy import create_engine
from config import DATABASE_URI
import doctors
import aux_services
from schemas import *
from sqlalchemy.orm import sessionmaker
import requests
import json
import string

engine = create_engine(DATABASE_URI)

Session = sessionmaker(bind=engine)


# geocoding api key = AIzaSyDOGA-EaxU3VaPEREE1FEmm37FLc_Rmyjk

# code to recreate data base
def recreate_database():
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)


# scrapes aux services API and populate data table with many instances
def populate_aux_services(num_per_city=30):
    # calls api with filters on 4 major cities: Austin, Dallas, San Antonio, and Houston
    # Austin:
    austin_url = (
        "https://data.medicare.gov/resource/ct36-nrcq.json?state=TX&city=AUSTIN"
    )
    payload = ""
    austin_response = requests.request("GET", austin_url, data=payload)
    services_in_austin = aux_services.parse_aux_data(austin_response)

    # Dallas:
    dallas_url = (
        "https://data.medicare.gov/resource/ct36-nrcq.json?state=TX&city=DALLAS"
    )
    dallas_response = requests.request("GET", dallas_url, data=payload)
    services_in_dallas = aux_services.parse_aux_data(dallas_response)

    # San Antonio:
    san_url = (
        "https://data.medicare.gov/resource/ct36-nrcq.json?state=TX&city=SAN+ANTONIO"
    )
    san_response = requests.request("GET", san_url, data=payload)
    services_in_san = aux_services.parse_aux_data(san_response)

    # Houston:
    houston_url = (
        "https://data.medicare.gov/resource/ct36-nrcq.json?state=TX&city=HOUSTON"
    )
    houston_response = requests.request("GET", houston_url, data=payload)
    services_in_houston = aux_services.parse_aux_data(houston_response)

    # get the presentable versions of the data and add sets to list ret
    res = []
    index = 1
    res.append(aux_services.present(services_in_austin, index))
    index += num_per_city
    res.append(aux_services.present(services_in_dallas, index))
    index += num_per_city
    res.append(aux_services.present(services_in_san, index))
    index += num_per_city
    res.append(aux_services.present(services_in_houston, index))

    s = Session()
    for city_info in res:
        for key in city_info.keys():
            # for each city's instances, get the information to persist to db
            values = city_info[key]
            company_name = values["name"]
            address = values["address"]
            city = values["city"]
            state = values["state"]
            zipcode = values["zip code"]
            phone = values["phone"]
            product = json.dumps(list(dict.fromkeys(values["product"])))
            image = values["image"]

            # create AuxServices object with obtained information
            aux_service = AuxServices(
                id=values["id"],
                company_name=string.capwords(company_name),
                address=string.capwords(address),
                city=string.capwords(city),
                state=state,
                zipcode=zipcode,
                phone=phone,
                product=product,
                image=image,
            )

            # add aux service instance to data table
            s.add(aux_service)
            s.commit()
    s.close()


# scrapes doctors API and populates doctor data table
def populate_doctors(q_lim=200, lim=30):
    # get the presentable response data for the 4 major cities
    _, doctor_data1 = doctors.get(city="austin", limit=q_lim, skip=0)
    _, doctor_data2 = doctors.get(city="dallas", limit=q_lim, skip=0)
    _, doctor_data3 = doctors.get(city="san antonio", limit=q_lim, skip=0)
    _, doctor_data4 = doctors.get(city="houston", limit=q_lim, skip=0)
    total_data = []
    # the data returned from API doesn't always correctly filter by city, so ensure
    # that each instance is the appropriate city
    # Austin:
    temp_lim = lim
    for doc in doctor_data1:
        # location API will break if '#' in address, so remove those instances
        if (
            "TX" in doc["state"]
            and "Austin" in doc["city"]
            and temp_lim > 0
            and "#" not in doc["street"]
            and doc["enumeration_type"] == "NPI-1"
        ):
            temp_lim -= 1
            total_data.append(doc)
    temp_lim = lim
    # Dallas:
    for doc in doctor_data2:
        if (
            "TX" in doc["state"]
            and "Dallas" in doc["city"]
            and temp_lim > 0
            and "#" not in doc["street"]
            and doc["enumeration_type"] == "NPI-1"
        ):
            temp_lim -= 1
            total_data.append(doc)
    temp_lim = lim
    # San Antonio:
    for doc in doctor_data3:
        if (
            "TX" in doc["state"]
            and "San Antonio" in doc["city"]
            and temp_lim > 0
            and "#" not in doc["street"]
            and doc["enumeration_type"] == "NPI-1"
        ):
            temp_lim -= 1
            total_data.append(doc)
    temp_lim = lim
    # Houston:
    for doc in doctor_data4:
        if (
            "TX" in doc["state"]
            and "Houston" in doc["city"]
            and temp_lim > 0
            and "#" not in doc["street"]
            and doc["enumeration_type"] == "NPI-1"
        ):
            temp_lim -= 1
            total_data.append(doc)
    s = Session()
    for doc in total_data:
        if doc["enumeration_type"] == "NPI-1":
            my_doc = Doctors(
                npi_number=doc["npi_number"],
                enumeration_type=doc["enumeration_type"],
                full_name=doc["full_name"],
                name=doc["name"],
                gender=doc["gender"],
                last_updated=doc["last_updated"],
                address=doc["street"],
                city=doc["city"],
                state=doc["state"],
                zipcode=doc["zip_code"],
                phone_number=doc["phone_number"],
                fax_number=doc["fax_number"],
                specialities=doc["specialities"],
            )

            s.add(my_doc)
            s.commit()
    s.close()


# Location class to call geocoding api
class Location:
    def __init__(self, address, city, state, id):
        self.address = address
        self.city = city
        self.state = state
        self.id = id

    # method to format address to pass to API easily
    def get_str(self):
        return self.address + ", " + self.city + ", " + self.state


# populates aux services coordinates and doctors coordinates tables
def populate_location_map():
    s = Session()
    # query all Doctors from doctor data table
    doc_result = s.query(Doctors).all()
    locations = set()
    for res in doc_result:
        location = Location(res.address, res.city, res.state, res.npi_number)
        locations.add(location)

    # using the addresses of each doctor, call api to get coordinates
    for loc in locations:
        full_address = loc.get_str()
        str_call = (
            "https://maps.googleapis.com/maps/api/geocode/json?address="
            + full_address
            + "&key=AIzaSyDOGA-EaxU3VaPEREE1FEmm37FLc_Rmyjk"
        )
        response = requests.request("GET", str_call).json()
        lat = response["results"][0]["geometry"]["location"]["lat"]
        lng = response["results"][0]["geometry"]["location"]["lng"]

        # add coordinates to doctor location map data table
        location_map = DoctorsLocationMap(
            address=loc.address,
            city=loc.city,
            state=loc.state,
            latitude=lat,
            longitude=lng,
            doctor_id=int(loc.id),
        )
        s.add(location_map)
        s.commit()
    locations = set()
    # do same thing for aux services, query all instances
    aux_result = s.query(AuxServices).all()
    for res in aux_result:
        location = Location(res.address, res.city, res.state, res.id)
        locations.add(location)
    # using the addresses, call goecoding api and get coordinates
    for loc in locations:
        full_address = loc.get_str()
        str_call = (
            "https://maps.googleapis.com/maps/api/geocode/json?address="
            + full_address
            + "&key=AIzaSyDOGA-EaxU3VaPEREE1FEmm37FLc_Rmyjk"
        )
        response = requests.request("GET", str_call).json()
        lat = response["results"][0]["geometry"]["location"]["lat"]
        lng = response["results"][0]["geometry"]["location"]["lng"]

        # add coordinates as instance to aux service location map table
        location_map = AuxServicesLocationMap(
            address=loc.address,
            city=loc.city,
            state=loc.state,
            latitude=lat,
            longitude=lng,
            aux_service_id=int(loc.id),
        )
        s.add(location_map)
        s.commit()
    s.close()


# transit record class contains these attributes to populate table
class TransitRecord:
    def __init__(
        self,
        doctor_id,
        doctor_lat,
        doctor_long,
        aux_id,
        aux_lat,
        aux_long,
        aux_city,
        doctor_city,
    ):
        self.doctor_id = doctor_id
        self.doctor_lat = doctor_lat
        self.doctor_long = doctor_long
        self.aux_id = aux_id
        self.aux_lat = aux_lat
        self.aux_long = aux_long
        self.aux_city = aux_city
        self.doctor_city = doctor_city


# populates join table for aux services and doctor coordinates
def populate_transit_connections(num_per_city=30):
    # to use for transit routes
    s = Session()
    doc_result = s.query(DoctorsLocationMap).all()
    connections = set()
    # the beginning values for each of the city's instances' ids
    austin_id = 1
    dallas_id = austin_id + num_per_city
    san_id = dallas_id + num_per_city
    houston_id = san_id + num_per_city
    for i in range(0, len(doc_result)):
        doc = doc_result[i]
        # for each city, get the corresponding city instance from aux services
        # to map to in transit table
        if "Austin" in doc.city:
            aux = (
                s.query(AuxServicesLocationMap)
                .filter(AuxServicesLocationMap.aux_service_id == austin_id)
                .first()
            )
            austin_id += 1
        elif "Dallas" in doc.city:
            aux = (
                s.query(AuxServicesLocationMap)
                .filter(AuxServicesLocationMap.aux_service_id == dallas_id)
                .first()
            )
            dallas_id += 1
        elif "San Antonio" in doc.city:
            aux = (
                s.query(AuxServicesLocationMap)
                .filter(AuxServicesLocationMap.aux_service_id == san_id)
                .first()
            )
            san_id += 1
        elif "Houston" in doc.city:
            aux = (
                s.query(AuxServicesLocationMap)
                .filter(AuxServicesLocationMap.aux_service_id == houston_id)
                .first()
            )
            houston_id += 1

        # put all info from connecting doctors and aux services to transit record
        # and add to set
        transit_record = TransitRecord(
            doc.doctor_id,
            doc.latitude,
            doc.longitude,
            aux.aux_service_id,
            aux.latitude,
            aux.longitude,
            aux.city,
            doc.city,
        )
        connections.add(transit_record)
    # add each transit record instance to transit connections data table
    for connection in connections:
        c = TransitConnections(
            aux_service_id=connection.aux_id,
            doctor_id=connection.doctor_id,
            aux_latitude=connection.aux_lat,
            aux_longitude=connection.aux_long,
            aux_city=connection.aux_city,
            doctor_latitude=connection.doctor_lat,
            doctor_longitude=connection.doctor_long,
            doctor_city=connection.doctor_city,
        )
        s.add(c)
        s.commit()
    s.close()


if __name__ == "__main__":
    recreate_database()
    populate_aux_services()
    populate_doctors()
    populate_location_map()
    populate_transit_connections()
