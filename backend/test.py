#!/usr/bin/env python3

from app.models.schemas import (
    Doctors,
    DoctorsLocationMap,
    AuxServices,
    AuxServicesLocationMap,
)
from unittest import main, TestCase


class MedtroTest(TestCase):
    def test_doctors_count(self):
        self.assertEqual(len(Doctors.query.all()), 120)

    def test_doctors_schema(self):
        sample_doctor = next(
            iter(Doctors.query.filter_by(npi_number=1093716375))
        ).__dict__
        self.assertEqual(len(sample_doctor) - 1, 13)

    def test_doctors_location_count(self):
        self.assertEqual(len(DoctorsLocationMap.query.all()), 120)

    def test_doctors_location_schema(self):
        for doctor_location in DoctorsLocationMap.query.all():
            self.assertEqual(len(doctor_location.__dict__), 7)

    def test_aux_services_count(self):
        self.assertEqual(len(AuxServices.query.all()), 120)

    def test_aux_services_schema(self):
        sample_aux_service = next(iter(AuxServices.query.filter_by(id=1))).__dict__
        self.assertEqual(len(sample_aux_service) - 1, 9)

    def test_aux_services_location_count(self):
        self.assertEqual(len(AuxServicesLocationMap.query.all()), 120)

    def test_aux_services_location_schema(self):
        for aux_service_location in AuxServicesLocationMap.query.all():
            self.assertEqual(len(aux_service_location.__dict__), 7)


if __name__ == "__main__":
    main()
