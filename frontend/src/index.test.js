import React, { StrictMode } from 'react'
import App from './App'

import { mount, render, shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('index.js test suite', () => {
    configure({ adapter: new Adapter() })
    it('tests if the app is rendered into the document', () => {
        expect(mount(
            <StrictMode data-test>
                <App />
            </StrictMode>
        ).find('.App').length).toBe(1)
    })
})
