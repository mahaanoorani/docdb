
import { formatAddress, mapGender, formatStatus } from './doctor_utils'

describe('doctor_utils.js test suite', () => {
    it('tests formatAddress()', () => {
        expect(formatAddress({
            address: '241 Qux St.',
            city: 'Fooville',
            state: 'State Bar',
            zipcode: '58541'
        })).toStrictEqual('241 Qux St.\nFooville, State Bar 58541')
    })
    it('tests mapGender()', () => {
        expect(mapGender('M')).toStrictEqual('Male')
        expect(mapGender('F')).toStrictEqual('Female')
    })
    it('tests formatStatus()', () => {
        expect(formatStatus('A')).toStrictEqual('Active')
        expect(formatStatus('I')).toStrictEqual('Inactive')
    })
})